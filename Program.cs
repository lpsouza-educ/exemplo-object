﻿using System;

namespace ExemploOO
{
    class Program
    {
        static void Main(string[] args)
        {
            Computador[] computadores = new Computador[1];

            for (int i = 0; i < computadores.Length; i++)
            {
                computadores[i] = new Computador();
                Console.WriteLine("Cadastro computador {0} de {1}", i+1, computadores.Length);
                Console.Write("Digite o número de cores: ");
                computadores[i].Cores = int.Parse(Console.ReadLine());
                Console.Write("Digite o total de memória: ");
                computadores[i].Memoria = int.Parse(Console.ReadLine());
                Console.Write("Digite o número de entrada USB: ");
                computadores[i].USBs = int.Parse(Console.ReadLine());
                Console.Write("Digite o número de entradas HDMI: ");
                computadores[i].HDMIs = int.Parse(Console.ReadLine());
                Console.Write("Digite o número de entradas VGA: ");
                computadores[i].VGAs = int.Parse(Console.ReadLine());
                Console.Write("Digite o nome do SO: ");
                computadores[i].SO = Console.ReadLine();
                Console.Write("Digite o número de série: ");
                computadores[i].NumSerie = Console.ReadLine();
            }

            Console.WriteLine("Antes de adicionar cores:");
            Console.WriteLine("|Cores\t|Mem\t|HDMIs\t|");
            for (int i = 0; i < computadores.Length; i++)
            {
                Console.WriteLine("|{0}\t|{1}\t|{2}\t|",
                computadores[i].Cores,
                computadores[i].Memoria,
                computadores[i].HDMIs);
            }

            Console.Write("Quantos cores você quer a mais? ");
            int nCores = int.Parse(Console.ReadLine());

            Console.WriteLine("Depois de adicionar cores:");
            Console.WriteLine("|Cores\t|Mem\t|HDMIs\t|");
            for (int i = 0; i < computadores.Length; i++)
            {
                computadores[i].AdicCore(nCores);
                computadores[i].Cores += nCores;
                
                Console.WriteLine("|{0}\t|{1}\t|{2}\t|",
                computadores[i].Cores,
                computadores[i].Memoria,
                computadores[i].HDMIs);
            }
        }
    }
}
