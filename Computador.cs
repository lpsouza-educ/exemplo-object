namespace ExemploOO
{
    public class Computador
    {
        public int Cores;
        public int Memoria;
        public int USBs;
        public int HDMIs;
        public int VGAs;
        public string SO;
        public string NumSerie;

        public void AdicCore(int novosCores)
        {
            // this.Cores = this.Cores + novosCores;
            this.Cores += novosCores;
        }
    }
}